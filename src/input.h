#ifndef INPUT_H
#define INPUT_H
#include <stdint.h>
#include <Arduino.h>
#include "config.pb.h"
#include <Adafruit_MCP23017.h>

// IO board will have 32 inputs one of which is the 3 way profile rocker
// and another is the dpad rocker leaving us with 29 possible button inputs
#define NUM_SPY_BTNS 29
#define MCP_MAX_PINS 16
#define MCP_DP 13 // bank b
#define MCP_PF1 14
#define MCP_PF3 15
#define MCP_A1 0
#define MCP_A2 1
#define HB_R2 6
#define HB_L2 7
#define MUX_MAX 6
// Allows us to throttle writes separate from reads
#define SEND_INTERVAL_MS 100
#define UPDATE_INTERVAL_MS 1
#define EX_UPDATE_INTERVAL_MS 10
#define ZERO '\0'  // Use a byte value of 0x00 to represent a bit with value 0.
#define ONE '1'    // Use an ASCII one to represent a bit with value 1.  This makes Arduino debugging easier.
#define SPLIT '\n' // Use a new-line character to split up the controller state packets
#define SBX_SPY_BTNS 23
#define XU_SPY_BTNS 20
#define HB_SPY_BTNS 18

#define GENERIC_SPY_BTNS 29
#define EX_DEFAULT_BAUD 19200
#define EX_DATA_BYTES 4
#define EX_BUFFER 8 * 2 // twice the size so i can read a chunk
#define EX_DP 7
#define EX_PF 3
#define EX_DATA_BEG 2
#define EX_DATA_END 7
#define IS_HB controller_type == Config_ControllerType_HIT_BOX
#define SBX_RST_BTN 22
#define HB_RST_BTN 14
#define RST_WAIT 1000
// Hit Box Button Order
// P1/Square: 0
// P2/Triangle: 1
// P3/R1: 2
// P4/R2: 3
// K1/X: 4
// K2/Circle: 5
// K3/R2: 6
// K4/L2: 7
// Up: 8
// Down: 9
// Left: 10
// Right: 11
// Share: 12
// TPKey: 13
// Options: 14
// PS: 15
// L3: 16
// R3: 17
static const byte sb_ex_mappings[] = {
    //ls u,d,l,r
    3, 1, 0, 2,

    //dp u,d,l,r
    19, 20, 18, 21,

    //rs u,d,r,l
    12, 15, 14, 13,
    // P
    4, 5, 6, 7,
    // K
    8, 9, 10, 11,
    // ST,EX,TLT
    22, 16, 17};

static const byte hb_bbg_mappings[] = {
    // P
    0, 1, 2, 3,
    // K
    4, 5, 6, 7,
    //ls l,r,d,u
    10, 11, 9, 8,
    // opt,share,ps r3
    14, 12, 15, 17,
    // l3, tp, tb
    16, 13, 18};
#define HB_BBG_MAPPING_LEN 19
// we will prolly only use this when we get analog stuff done so we have both axes, but
// for meow this is how we are gonna do it
static const byte xu_bbg_mappings[] = {
    // P
    0, 1, 2, 3,
    // K
    4, 5, 6, 7,
    //ls l,r,d,u
    10, 11, 9, 8,
    // opt,share,ps r3
    14, 12, 15, 17,
    // l3, tp,
    16, 13,
    // allow for an xu style controller, and using the xu theme
    // gui can check to see if these buttons are mapped, if one of them is auto use xu theme?
    // rs l,r,d,u
    21, 22, 20, 19,

    // tb
    18};
#define XU_BBG_MAPPING_LEN 23

class Input
{
public:
    // returns the current profile the controller is on
    uint8_t currentProfile()
    {
        return current_profile;
    }
    // returns whether the dpad toggle is on
    bool isDpadEnabled()
    {
        return dpad_enabled;
    }

    uint32_t lastUpdateMillis()
    {
        return last_update;
    }
    virtual bool close()
    {
        return true;
    }

    void checkReset();

    virtual void loop();
    virtual bool supportsVariants() { return false; }

    // writes spy state to usb serial every `SEND_INTERVAL_MS` milliseconds
    // returns true if sent
    virtual bool getButtonState(uint8_t btn);

    bool sendState();
    bool setVariant(Config_ControllerVariant tp)
    {
        variant = tp;
        return onControllerVariantChange();
    }
    bool setControllerType(Config_ControllerType tp)
    {
        controller_type = tp;
        return onControllerTypeChange();
    };

protected:
    virtual bool onControllerTypeChange() { return true; };
    virtual bool onControllerVariantChange() { return true; };
    uint32_t last_send;
    uint8_t current_profile;
    bool dpad_enabled;
    uint32_t last_update;
    Config_ControllerType controller_type;
    Config_ControllerVariant variant;
    // returns the state of the selected button. If the button is not supported by the input
    // it should return false
};

class MCP : public Input
{
public:
    MCP(uint8_t a1 = MCP_A1, uint8_t a2 = MCP_A2);
    bool getButtonState(uint8_t btn)
    {
        if (btn >= SBX_SPY_BTNS)
            return false;
        return buttons[btn];
    }

    void loop()
    {
        readState();
    }

    bool readState();
    bool supportsVariants() { return true; }

protected:
    bool onControllerTypeChange();

private:
    Adafruit_MCP23017 mcp_a;
    Adafruit_MCP23017 mcp_b;
    uint16_t last_state_a;
    uint16_t last_state_b;

    bool buttons[NUM_SPY_BTNS];
};

class DEBUG : public Input
{
public:
    DEBUG(bool st = false) { state = false; }
    bool getButtonState(uint8_t btn)
    {
        return state;
    }

    void loop()
    {
    }

private:
    bool state;
};

typedef uint32_t ex_state_t;

// TODO: figure out what to do for different controllers. just gonna use smash box
// for now ig.
class EX : public Input
{
public:
    EX();
    void loop()
    {
        if (readEXState())
            parse();
    }
    bool getButtonState(uint8_t btn)
    {
        if (btn >= SBX_SPY_BTNS)
        {
            return false;
        }
        return buttons[btn];
    }
    bool close();

private:
    bool parse();
    byte raw[EX_BUFFER];
    byte data[EX_DATA_BYTES];
    bool readEXState();
    bool controllerTypeValid();
    void setButton(uint8_t idx, bool val);
    uint32_t last_update;

    bool buttons[SBX_SPY_BTNS];
};

#define BBG_MSI 0x40 // Main switch input
#define BBG_SSI 0x41 // Secondary switch inputs

#define BBG_R3 7
#define BBG_L3 6
#define BBG_TP 5
#define BBG_TB 4
#define BBG_LK 3 // should be dpad switch
#define BBG_DP 2 // Profile 0
#define BBG_RS 1 // Profile 1
#define BBG_LS 0 // Profile 2
#define BBG_DEFAULT_BAUD 19200

class BBG : public Input
{
public:
    BBG();
    void loop()
    {
        //TODO: do i need to throttle this or do i just hammer it
        readBBGState();
    }
    bool close();
    bool getButtonState(uint8_t btn)
    {
        if (btn >= SBX_SPY_BTNS)
        {
            return false;
        }
        return buttons[btn];
    }
    bool supportsVariants() { return true; }

private:
    bool parse();
    byte raw[EX_BUFFER];
    byte data[EX_DATA_BYTES];
    bool readBBGState();
    bool controllerTypeValid();
    void setButton(uint8_t idx, bool val);
    uint32_t last_update;

    bool buttons[SBX_SPY_BTNS];
};
static inline byte numButtons(Config_ControllerType tp, Config_ControllerVariant var)
{
    switch (tp)
    {
    case Config_ControllerType_SMASH_BOX:
        return SBX_SPY_BTNS;
        break;
    case Config_ControllerType_CROSSUP:
        return XU_SPY_BTNS;
    case Config_ControllerType_HIT_BOX:
        return HB_SPY_BTNS;
    case Config_ControllerType_GENERIC:
    case Config_ControllerType_BBG_NOSTALGIA:
        switch (var)
        {
        case Config_ControllerVariant_OTHER:
            return GENERIC_SPY_BTNS;
        case Config_ControllerVariant_FIGHTSTICK:
        case Config_ControllerVariant_STICKLESS:
            return HB_SPY_BTNS;
        case Config_ControllerVariant_FIGHTSTICK_EXD:
            return XU_SPY_BTNS;
        }
    }
    return GENERIC_SPY_BTNS;
}

#endif