#define FASTLED_ALLOW_INTERRUPTS 0
#include <Arduino.h>
#include "input.h"
#include "config.h"
#include <Wire.h>
#include "led.h"
#define OUT_BAUD 115200
//#define DEBUG_INPUT
//#define SERIAL_WAIT

Input *input;
ConfigManager *cm;
Input *onControllerTypeChange(Config_ControllerType tp)
{
  input->close();
  switch (tp)
  {
  case Config_ControllerType_BBG_NOSTALGIA:
    input = new BBG();
    break;
  case Config_ControllerType_CROSSUP:
  case Config_ControllerType_SMASH_BOX:
    input = new EX();
    break;
  default:
    input = new MCP();
  }
  return input;
}
void setup()
{
  Serial.begin(OUT_BAUD);
#ifdef SERIAL_WAIT
  while (!Serial)
    ;
#endif
#ifdef __AVR__
  input = new DEBUG();
#else
  Wire.begin();
  Wire.beginTransmission(MCP23017_ADDRESS | MCP_A1);
  uint8_t isMCP = Wire.endTransmission();
  Wire.end();
#ifndef DEBUG_INPUT
  if (isMCP == 0)
  {
    input = new MCP();
  }
  else
  {
    input = new EX();
  }
#else
  input = new DEBUG(true);
#endif
#endif


  cm = new ConfigManager(input, onControllerTypeChange);
  cm->readConfig();
  
  input->checkReset();

}

void loop()
{
  if (input == NULL)
  {
    cm->error(ERR_NULL_PTR, "input is null");
    return;
  }
  cm->loop();
  input->loop();
}