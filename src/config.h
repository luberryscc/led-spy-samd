#ifndef CONFIG_H
#define CONFIG_H
#include <config.pb.h>
#include "led.h"
#include "input.h"
#include <Arduino.h>
#define CMD_WR "WR"
#define CMD_RD "RD"
#define CMD_STATE "STATE"
#define CMD_SEP ":"
#define CMD_OK "OK"
#define CMD_READY "READY"
#define CMD_RELOAD "RL"
#define CMD_FAIL "FAIL"
#define CMD_BTN_TEST "BTNT"
#define CMD_FIREHOSE "FHOSE"
#define CMD_INF "INF"
#ifdef __AVR__
#define MAX_CFG_SIZE 2048
#else
#define MAX_CFG_SIZE 4096
#endif
#define SIG "MIKU"
#define SIG_LEN 5
#define CMD_TIMEOUT 1000
#define EEPROM_EMULATION_SIZE MAX_CFG_SIZE + SIG_LEN

typedef enum
{
    ERR_CMD_PARSE,
    ERR_CFG_LEN,
    ERR_DECODE,
    ERR_CRC_MISMATCH,
    ERR_EEPROM_WRITE,
    ERR_EEPROM_READ,
    ERR_CFG_SEND,
    ERR_NULL_PTR,
    ERR_LED_CFG,
    ERR_CMD_TIMEOUT,
    ERR_IDX_OUT_OF_RANGE,
    ERR_INT_STORAGE_OPEN,
} error_t;

typedef struct
{
    size_t len;
    void *data;
} pb_list_t;

//TODO make this c. cpp doesn't make sense here.
// also decipher how tf to use this shit https://github.com/nanopb/nanopb/blob/master/examples/using_union_messages/decode.c
// need to use the nanopb-arduino stream bs

class ConfigManager
{
public:
    void loop();
    // reads config from eeprom into protobuf
    bool readConfig();

    bool sendConfig();

    ConfigManager(Input *in, Input *(*onTypeChange)(Config_ControllerType tp));
    // TODO break these out
    void error(error_t type, String msg = "");
    void ok(String msg = "");
    void ready(String msg = "");

private:
    uint8_t rawCfg[MAX_CFG_SIZE];
    Input *(*onChange)(Config_ControllerType tp);
    uint16_t rawCfgLen;
    uint32_t rawCfgCRC;
    bool writeRawCfg();
    bool defaultCfg();
    bool readRawCfg();
    bool firehose;
    // creates an led object populated with profiles from configuration
    bool configLED();
    Config cfg;
    LED led;
    String inputString;  // a string to hold incoming data
    bool stringComplete; // whether the string is complete
    Input *input;
    uint8_t testLED;
};

#endif