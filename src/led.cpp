#include <Arduino.h>
#include "led.h"
#include <FastLED.h>

/**
  -----------------------------------------------------
  Set both LEDs for the given button (lower index) to the provided colors
  -----------------------------------------------------
*/
bool LED::setLEDs(uint8_t raw_index, Profile *prof)
{
    bool changed = false;
    if (prof == NULL)
    {
        return changed;
    }
    color_t color = {0,
                     0,
                     0};
    uint8_t index = getMapping(raw_index);
    // don't set disabled buttons, test led, or out of range buttons
    if (index != UINT8_MAX && !prof->isDisabled(index) && index != test_led)
        color = prof->getColor(raw_index, input->getButtonState(index));

    uint8_t id = raw_index * 2;
    CRGB *led = &leds[id];
    changed = led->r != color.r || led->g != color.g || led->b != color.b;
    led->r = color.r;
    led->g = color.g;
    led->b = color.b;
    if ((id + 1) < GENERIC_SPY_BTNS * 2)
    {
        led = &leds[id + 1];
        led->r = leds[id].r;
        led->g = leds[id].g;
        led->b = leds[id].b;
    }
}

Profile *LED::getProfile(uint8_t idx)
{
    // TODO maybe return null tho unset profiles should be null. maybe check this
    if (idx >= num_profiles)
    {
        return profiles[0];
    }
    return profiles[idx];
}

uint8_t LED::getMapping(uint8_t raw_idx)
{
    if (raw_idx >= GENERIC_SPY_BTNS)
        return UINT8_MAX;

    uint8_t val = mappings[raw_idx];
    if (val >= GENERIC_SPY_BTNS)
        return UINT8_MAX;
    return val;
}

/**
  -----------------------------------------------------
  breath will modify the values of the leds to adjust the brightness for a breathing function
  -----------------------------------------------------
*/
void LED::breathe(bool enabled, float interval)
{
    // TODO: maybe theres a better way to do this.
    if (enabled)
    {
        if (bright == default_bright)
        {
            return;
        }
        bright = default_bright;
        convertedBright = ceil(255 * bright);
        FastLED.delay(3);
        return;
    }

    if (!breathe_down)
    {
        bright += interval;
        if (bright >= max_bright)
        {
            breathe_down = true;
            bright = max_bright;
        }
    }
    else
    {
        bright -= interval;
        if (bright <= min_bright)
        {
            breathe_down = false;
            bright = min_bright;
        }
    }
    convertedBright = ceil(255 * bright);
}

bool LED::setCurrentProfile()
{

    // get the profile id from the state variable
    uint8_t pid = input->currentProfile();

    // if the profile switch changes values switch to that switch profile's default lighting profile
    if (last_profile != pid && pid < num_profiles)
    {
        last_profile = current_profile;
        current_profile = pid;
        FastLED.delay(150);
    }
    return true;
}

/**
  -----------------------------------------------------
  Led Control Set our LED profiles based on switches/hotkeys as well as brightness,
  takes the state of the switches to allow for hotkeys and such
  -----------------------------------------------------
*/

void LED::loop()
{
    if (last_cfg_crc == 0 || FastLED.count() != 1)
        return;
    // FastLED.delay(3);
    setCurrentProfile();
    // TODO: handle hotkeys again
    //  will probably be config based tbh

    Profile *pf = getProfile(current_profile);
    if (pf == NULL)
        return;
    Config_Profile_Animation animation = pf->getAnimation();
    // allows any profile to breathe as long as it is enabled
    breathe(animation == Config_Profile_Animation_BREATHE, pf->breatheInterval());
    // calls the current profile's run method which will setup the leds
    bool changed = FastLED.getBrightness() != ceil(255 * bright) || last_update == 0 || max(millis() - last_update, 0) < FORCE_UPDATE_INT;
    for (uint8_t i = 0; i < GENERIC_SPY_BTNS; i++)
        if (setLEDs(i, pf))
            changed = true;
    if (!changed)
        return;
    last_update = millis();

    FastLED.setBrightness(ceil(255 * bright));
    FastLED.show();
}

bool LED::addProfile(Profile *p)
{
    if (num_profiles >= MAX_PROFILES)
    {
        return false;
    }
    profiles[num_profiles] = p;
    num_profiles++;
    return true;
}

bool LED::addSwitchProfile(uint8_t pf_idx)
{
    if (num_switch_profiles + 1 > MAX_PROFILES || pf_idx >= num_profiles)
    {
        return false;
    }
    switch_profile_ids[num_switch_profiles] = pf_idx;
    num_switch_profiles++;
    return true;
}

LED::LED(float brightness,
         float max_brightness,
         uint8_t mp[GENERIC_SPY_BTNS],
         Input *in,
         uint32_t crc,
         float min_brightness)
{
    num_profiles = 0;
    num_switch_profiles = 0;
    FastLED.setMaxPowerInVoltsAndMilliamps(5, 800);
    for (uint8_t i = 0; i < MAX_PROFILES; i++)
    {
        profiles[i] = NULL;
    }
    reconfigure(brightness, max_brightness, mp, in, crc, min_brightness);
}

bool LED::reconfigure(float brightness,
                      float max_brightness,
                      uint8_t mp[GENERIC_SPY_BTNS],
                      Input *in,
                      uint32_t crc,
                      float min_brightness,
                      byte num_buttons)
{
    if (num_buttons > 0)
    {
        if (FastLED.count() < 1)
            FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, num_buttons * 2);
        else if (FastLED[0].size() != num_buttons)
            NVIC_SystemReset();
    }
    last_update = 0;
    last_cfg_crc = crc;
    current_profile = 0;
    bright = brightness;
    convertedBright = ceil(255 * bright);
    max_bright = max_brightness;
    default_bright = brightness;
    min_bright = min_brightness;
    breathe_down = true;
    input = in;
    if (num_profiles > MAX_PROFILES)
    {
        // prevent possible stack smash;
        num_profiles = MAX_PROFILES;
    }
    for (int i = 0; i < num_profiles; i++)
    {
        if (profiles[i] == NULL)
            continue;
        profiles[i] = NULL;
    }
    num_profiles = 0;
    num_switch_profiles = 0;
    test_led = UINT8_MAX;
    if (mp == NULL)
    {
        return false;
    }
    for (int i = 0; i < GENERIC_SPY_BTNS; i++)
    {
        mappings[i] = mp[i];
    }
    return true;
}

Static::Static(
    Config_Profile_Animation anim,
    float bi,
    uint32_t disabled,
    color_t c[GENERIC_SPY_BTNS])
{
    animation = anim;
    breathe_interval = bi;
    disabled_bitfield = disabled;
    for (int i = 0; i < GENERIC_SPY_BTNS; i++)
    {
        colors[i] = c[i];
    }
}

color_t Static::getColor(uint8_t idx, bool)
{
    color_t color;
    if (idx < GENERIC_SPY_BTNS)
        color = colors[idx];
    return color;
}

Reactive::Reactive(
    Config_Profile_Animation anim,
    float bi,
    uint32_t disabled,
    color_t c[GENERIC_SPY_BTNS],
    color_t cr[GENERIC_SPY_BTNS])
{
    animation = anim;
    breathe_interval = bi;
    disabled_bitfield = disabled;
    for (int i = 0; i < GENERIC_SPY_BTNS; i++)
    {
        colors[i] = c[i];
        r_colors[i] = cr[i];
    }
}

color_t Reactive::getColor(uint8_t idx, bool state)
{
    color_t color;
    if (idx < GENERIC_SPY_BTNS)
        color = state ? r_colors[idx] : colors[idx];
    return color;
}