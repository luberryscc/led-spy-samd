#ifndef LED_H
#define LED_H
#include <Arduino.h>
#include "input.h"
#include <FastLED.h>

#ifndef DATA_PIN
#ifdef __AVR__
#define DATA_PIN 9
#else
#define DATA_PIN 3
#endif
#endif

#define MAX_PROFILES 12 // this is based on the number of profiles crossup has on board for hotkey mode
#define MIN_BRIGHTNESS 0.0
#define DEFAULT_BRIGHT 0.5
#define DEFAULT_MAX_BRIGHT 1.0
#define DEFAULT_MIN_BRIGHT MIN_BRIGHTNESS
#define FORCE_UPDATE_INT 1000

#define DEFAULT_BREATHE_INT 0.001

typedef struct color_t
{
    byte r : 8;
    byte g : 8;
    byte b : 8;
} color_t;

class Profile
{
public:
    float breatheInterval()
    {
        return breathe_interval;
    }
    Config_Profile_Animation getAnimation()
    {
        return animation;
    }
    virtual color_t getColor(uint8_t btn, bool state);
    bool isDisabled(uint8_t btn)
    {
        if (btn < NUM_SPY_BTNS)
            return bitRead(disabled_bitfield, btn);
        return false;
    }

protected:
    float breathe_interval;
    Config_Profile_Animation animation;
    uint32_t disabled_bitfield;
};

class LED
{
public:
    LED(float brightness = DEFAULT_BRIGHT,
        float max_brightness = DEFAULT_MAX_BRIGHT,
        uint8_t mp[GENERIC_SPY_BTNS] = {},
        Input *input = NULL,
        uint32_t crc = 0,
        float min_brightness = MIN_BRIGHTNESS);
    bool reconfigure(float brightness,
                     float max_brightness,
                     uint8_t mp[GENERIC_SPY_BTNS],
                     Input *input,
                     uint32_t crc,
                     float min_brightness = MIN_BRIGHTNESS,
                     byte num_buttons = 0);
    void loop();
    Input *getInput()
    {
        return input;
    }

    uint8_t getMapping(uint8_t raw_idx);

    bool addProfile(Profile *p);
    bool addSwitchProfile(uint8_t pf_idx);

    bool testLED(uint8_t idx)
    {
        if (idx >= GENERIC_SPY_BTNS * 2)
            return false;
        test_led = idx;
        return true;
    };
    void resume() { test_led = UINT8_MAX; };
    uint32_t getLastCFGCRC() { return last_cfg_crc; };
    uint8_t getTestLED() { return test_led; };

private:
    uint32_t last_cfg_crc;
    uint8_t test_led;
    Input *input;
    // list of profiles
    Profile *profiles[MAX_PROFILES];
    uint8_t switch_profile_ids[MAX_PROFILES];
    uint8_t current_profile;
    uint8_t num_profiles;
    uint8_t num_switch_profiles;
    uint8_t mappings[GENERIC_SPY_BTNS];
    float bright;
    byte convertedBright;
    float default_bright;
    float max_bright;
    float min_bright;
    bool breathe_down;
    uint32_t last_update;
    uint8_t last_profile;
    CRGB leds[GENERIC_SPY_BTNS * 2];
    Profile *getProfile(uint8_t idx);

    bool setCurrentProfile();

    /****LED control helpers****/
    // this will set the leds at the given index to the color returned
    bool setLEDs(uint8_t raw_index, Profile *prof);

    /****Animations****/
    // breath will cause any profile to have a
    // breath animation if the given interval > 0
    void breathe(bool enabled, float interval);
    /******************/
};

class Static : public Profile
{
public:
    Static(
        Config_Profile_Animation animation,
        float breathe_interval,
        uint32_t disabled,
        color_t colors[GENERIC_SPY_BTNS]);
    color_t getColor(uint8_t btn, bool state);

private:
    // list of colors to set each button to
    color_t colors[GENERIC_SPY_BTNS];
};
class Reactive : public Profile
{
public:
    Reactive(
        Config_Profile_Animation animation,
        float breathe_interval,
        uint32_t disabled,
        color_t colors[GENERIC_SPY_BTNS],
        color_t r_colors[GENERIC_SPY_BTNS]);

    color_t getColor(uint8_t btn, bool state);

private:
    // list of colors to set each button to
    color_t colors[GENERIC_SPY_BTNS];
    color_t r_colors[GENERIC_SPY_BTNS];
};

#endif