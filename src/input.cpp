#include "input.h"

static byte binData[] = {0x0,
                         0x0,
                         0x0,
                         0x0,
                         0x0};
bool Input::sendState()
{
    /*if (max(millis() - last_send, 0) < SEND_INTERVAL_MS)
    {
        return false;
    }*/
    int j = 0;
    int k = 0;
    for (int i = 0; i < NUM_SPY_BTNS; j++)
        for (k = 0; k < 8 && i < NUM_SPY_BTNS; i++, k++)
            binData[j] = bitWrite(binData[j], k, getButtonState(i));

    binData[j - 1] = bitWrite(binData[j - 1], k, isDpadEnabled());
    binData[j] = currentProfile();
    Serial.write(binData, 5);
    Serial.write(5);
    Serial.write(7);
    Serial.write(2);
    return true;
}

void Input::checkReset()
{
    byte rb = -1;
    switch (controller_type)
    {
    case Config_ControllerType_SMASH_BOX:
        rb = SBX_RST_BTN;
        break;
    case Config_ControllerType_GENERIC:
    case Config_ControllerType_HIT_BOX:
    case Config_ControllerType_CROSSUP:
    case Config_ControllerType_BBG_NOSTALGIA:
        rb = HB_RST_BTN;
        break;
    }
    loop();

    if (rb < 0 || !getButtonState(rb))
        return;
    delay(1000);
    loop();
    if (!getButtonState(rb))
        return;
    *((volatile uint32_t *)(HMCRAMC0_ADDR + HMCRAMC0_SIZE - 4)) = 0xf01669ef;
    NVIC_SystemReset();
}

MCP::MCP(uint8_t a1, uint8_t a2)
{
    Wire.setClock(400000);
    for (int i = 0; i < GENERIC_SPY_BTNS; i++)
        buttons[i] = false;
    controller_type = Config_ControllerType_GENERIC;
    mcp_a.begin(a1);
    mcp_b.begin(a2);
    for (uint8_t i = 0; i < 16; i++)
    {
        mcp_a.pinMode(i, INPUT);
        mcp_a.pullUp(i, HIGH);
        mcp_b.pinMode(i, INPUT);
        mcp_b.pullUp(i, HIGH);
    }
    last_state_a = 0ul;
    last_state_b = 0ul;
    last_update = 0;
    readState();
}
bool MCP::onControllerTypeChange()
{
    for (uint8_t i = 0; i < 16; i++)
    {
        mcp_a.pinMode(i, INPUT);
        mcp_a.pullUp(i, HIGH);
        mcp_b.pinMode(i, INPUT);
        mcp_b.pullUp(i, HIGH);
    }
    if (IS_HB)
    {
        // Invert L2 and R2 since they are active high for hit box
        mcp_a.pullUp(HB_L2, LOW);
        mcp_a.pullUp(HB_R2, LOW);
    }
    return true;
}

bool MCP::readState()
{
    if (max(millis() - last_update, 0) < UPDATE_INTERVAL_MS)
    {
        return false;
    }
    uint32_t a = mcp_a.readGPIOAB();
    uint32_t b = mcp_b.readGPIOAB();
    if (a == last_state_a && b == last_state_b)
    {
        return false;
    }
    last_state_a = a;
    last_state_b = b;
    for (uint8_t i = 0; i < MCP_MAX_PINS; i++)
    {
        buttons[i] = (IS_HB && (i == HB_L2 || i == HB_R2)) ? bitRead(a, i) : !bitRead(a, i);
        uint8_t bi = i + MCP_MAX_PINS;
        if (bi < NUM_SPY_BTNS)
        {
            buttons[bi] = !bitRead(b, i);
        }
    }
    if (IS_HB)
    {
        //TODO: we may have hotkeys for this shit
        current_profile = 0;
        dpad_enabled = false;

        last_update = millis();
        return true;
    }
    dpad_enabled = !bitRead(b, MCP_DP);
    bool pf1 = !bitRead(b, MCP_PF1);
    bool pf3 = !bitRead(b, MCP_PF3);
    if (pf1)
    {
        current_profile = 0;
    }
    else if (pf3)
    {
        current_profile = 2;
    }
    else
    {
        current_profile = 1;
    }
    last_update = millis();
    return true;
}

EX::EX()
{
    for (int i = 0; i < GENERIC_SPY_BTNS; i++)
        buttons[i] = false;
    last_update = 0;
    Serial1.begin(EX_DEFAULT_BAUD);
}
bool EX::close()
{
    Serial1.end();
    return true;
}
void EX::setButton(uint8_t btn, bool val)
{
    if (btn > SBX_SPY_BTNS)
    {
        return;
    }
    switch (controller_type)
    {
    case Config_ControllerType_SMASH_BOX:
        buttons[sb_ex_mappings[btn]] = val;
        return;
    case Config_ControllerType_CROSSUP:

        // TODO: Add XU mappings
        // make sure to short circut if > mappings for xu
        return;
    default:
        return;
    }
}
bool EX::parse()
{
    if (max(millis() - last_update, 0) < UPDATE_INTERVAL_MS)
    {
        return false;
    }
    int btn = 0;
    for (int i = 0; i < EX_DATA_BYTES - 1; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (i == EX_DATA_BYTES - 2 && j == EX_DP)
            {
                dpad_enabled = bitRead(data[i], j);
                continue;
            }
            setButton(btn, bitRead(data[i], j));
            btn++;
        }
    }
    current_profile = data[EX_PF] - 1;
    last_update = millis();
    return true;
}
bool EX::controllerTypeValid()
{
    return controller_type == Config_ControllerType_CROSSUP || controller_type == Config_ControllerType_SMASH_BOX;
}
bool EX::readEXState()
{
    if (!controllerTypeValid())
        return false;
    if (!Serial1.available())
    {
        return false;
    };
    while (Serial1.available() < EX_BUFFER)
        delay(1);

    for (byte i = 0; i < EX_BUFFER; i++)
        raw[i] = Serial1.read();

    byte start = 0;
    bool found = false;
    for (start = 0; start < EX_BUFFER / 2; start++)
        if (raw[start] == 5)
        {
            if (raw[start + 1] != 7)
            {
                continue;
            }
            start += 1;
            if (raw[start + 1] != 2)
            {
                continue;
            }
            start += 2;
            found = true;
            break;
        }
    if (!found)
        return false;
    byte sum = 0;
    for (byte i = 0; i < EX_DATA_BYTES; i++)
        sum += raw[i + start];
    if (sum != raw[start + EX_DATA_BYTES])
        return false;

    for (int i = 0; i < EX_DATA_BYTES && start < EX_BUFFER; i++, start++)
    {
        data[i] = raw[start];
    }

    return true;
}
BBG::BBG()
{
    for (int i = 0; i < GENERIC_SPY_BTNS; i++)
        buttons[i] = false;
    last_update = 0;
    Serial1.begin(BBG_DEFAULT_BAUD);
}
bool BBG::close()
{
    Serial1.end();
    return true;
}
void BBG::setButton(uint8_t btn, bool val)
{
    if (btn > SBX_SPY_BTNS)
    {
        return;
    }
    switch (variant)
    {
    case Config_ControllerVariant_OTHER:
        buttons[btn] = val;
        return;
    case Config_ControllerVariant_STICKLESS:
    case Config_ControllerVariant_FIGHTSTICK:
        if (btn >= HB_BBG_MAPPING_LEN)
            return;
        buttons[hb_bbg_mappings[btn]] = val;
        return;
    case Config_ControllerVariant_FIGHTSTICK_EXD:
        if (btn >= XU_BBG_MAPPING_LEN)
            return;
        buttons[xu_bbg_mappings[btn]] = val;
        return;
    }
}
bool BBG::controllerTypeValid()
{
    return controller_type == Config_ControllerType_BBG_NOSTALGIA;
}
bool BBG::readBBGState()
{
    if (!controllerTypeValid())
        return false;

    Serial1.write(BBG_MSI);
    uint8_t tries = 0;
    while (Serial1.available() < 2 && tries < 10)
    {
        delay(1);
        tries++;
    }
    uint16_t msil = Serial1.read();
    uint16_t msir = Serial1.read();
    uint16_t msi = (msil << 8) | msir;

    Serial1.write(BBG_SSI);
    tries = 0;
    while (Serial1.available() < 1 && tries < 10)
    {
        delay(1);
        tries++;
    }
    uint8_t ssi = Serial1.read();
    uint8_t i = 0;
    for (; i < 15; i++)
    {
        setButton(i, !bitRead(msi, i));
    }

    setButton(i++, !bitRead(ssi, BBG_R3));
    setButton(i++, !bitRead(ssi, BBG_L3));
    setButton(i++, !bitRead(ssi, BBG_TP));
    setButton(i++, !bitRead(ssi, BBG_TB));

    dpad_enabled = !bitRead(ssi, BBG_LK);
    current_profile = 1;
    if (!bitRead(ssi, BBG_LS))
        current_profile = 0;
    else if (!bitRead(ssi, BBG_RS))
        current_profile = 2;
}