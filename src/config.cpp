#include <config.h>
#include "led.h"
#include "input.h"
#include <Arduino.h>
#include "config.pb.h"
#include <pb_encode.h>
#include <pb_decode.h>
#include <CRC32.h>
#ifdef __AVR__
#include <EEPROM.h>

#define printf(...)            \
    char buf[256];             \
    sprintf(buf, __VA_ARGS__); \
    Serial.print(buf);

#else
#include <FlashAsEEPROM_SAMD.h>
#define printf Serial.printf
#endif
#define CONFIG_IS_ZERO(c) c.mappings.size == 0
#define TO_COLOR(idx, b)               \
    {                                  \
        b[idx], b[idx + 1], b[idx + 2] \
    }

#define CMD_ERR(code)           \
    {                           \
        error(code);            \
        stringComplete = false; \
        inputString = "";       \
        return;                 \
    }
#define CMD_ERR_MSG(code, msg)  \
    {                           \
        error(code, msg);       \
        stringComplete = false; \
        inputString = "";       \
        return;                 \
    }
#define FROM_COLOR(idx, bf, c) \
    bf[idx] = c.r;             \
    bf[idx + 1] = c.g;         \
    bf[idx + 2] = c.b;         \
    bf[idx + 3] = 0;

const static byte defConfig[] = {0xd, 0x0, 0x0, 0x0, 0x3f, 0x15, 0x0, 0x0, 0x80, 0x3f, 0x22, 0x17, 0x14, 0x13, 0x11, 0x12, 0xa, 0x9, 0x8, 0x7, 0x6, 0x5, 0x4, 0x3, 0xb, 0x2, 0xc, 0x1, 0x0, 0x15, 0xf, 0x10, 0xe, 0xd, 0x16, 0x28, 0x3, 0x32, 0x65, 0x15, 0x6f, 0x12, 0x83, 0x3a, 0x1a, 0x5e, 0xa, 0x5c, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0x32, 0xc6, 0x1, 0x8, 0x1, 0x15, 0x6f, 0x12, 0x83, 0x3a, 0x22, 0xbc, 0x1, 0xa, 0x5c, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0x12, 0x5c, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0xff, 0x32, 0x60, 0x1a, 0x5e, 0xa, 0x5c, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0x0, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0xff, 0x0, 0xff, 0xff, 0xff, 0xff, 0xff, 0x3a, 0x3, 0x0, 0x1, 0x2, 0x40, 0x1};
const static size_t defConfigLen = sizeof(defConfig) / sizeof(byte);

// migrates legacy animation in config to new animation.
// (don't think we will actually need this because of the fake eeprom but its here)
static inline void migrateAnimation(_Config_Profile *prof)
{
    if (prof->animation != Config_Profile_Animation_NIL)
        return;
    if (prof->breathe_interval <= 0)
    {
        prof->animation = Config_Profile_Animation_NONE;
        return;
    }
    prof->animation = Config_Profile_Animation_BREATHE;
    return;
}
static inline bool pbProfileDecode(pb_istream_t *stream, const pb_field_t *field, void **arg)
{
    if (stream == NULL || field->tag != Config_profiles_tag)
    {
        return true;
    }

    // is this the correct thing for arg??
    pb_list_t *pbList = (pb_list_t *)*arg;
    if (pbList == NULL || pbList->data == NULL)
    {
        return true;
    }

    _Config_Profile *data = &(((_Config_Profile *)pbList->data)[pbList->len]);

    if (!pb_decode(stream, Config_Profile_fields, data))
    {
        Serial.println(PB_GET_ERROR(stream));
        return false;
    }
    migrateAnimation(data);
    pbList->len++;
    return true;
}
static inline Profile *toLEDProfile(const _Config_Profile *prof)
{
    switch (prof->type)
    {
    case Config_ProfileType_REACTIVE:
    {
        // THIS MAY BE THE CULPRIT.
        color_t colors[GENERIC_SPY_BTNS], r_colors[GENERIC_SPY_BTNS];
        for (int i = 0, j = 0; j < prof->preactive.colors.size; i++, j += 4)
        {
            colors[i] = TO_COLOR(j, prof->preactive.colors.bytes);
            r_colors[i] = TO_COLOR(j, prof->preactive.react_colors.bytes);
        }
        return new Reactive(prof->animation, prof->breathe_interval, prof->disabled_buttons, colors, r_colors);
    }
    case Config_ProfileType_STATIC:
    {
        color_t colors[GENERIC_SPY_BTNS];
        for (int i = 0, j = 0; j < prof->pstatic.colors.size; i++, j += 4)
        {
            colors[i] = TO_COLOR(j, prof->pstatic.colors.bytes);
        }

        return new Static(prof->animation, prof->breathe_interval, prof->disabled_buttons, colors);
    }
    }
    return NULL;
}

ConfigManager::ConfigManager(Input *in, Input *(*onTypeChange)(Config_ControllerType tp))
{
    input = in;
    onChange = onTypeChange;
    rawCfgLen = 0;
    rawCfgCRC = 0;
    stringComplete = false;
    inputString = "";
    cfg = Config_init_zero;
#ifndef __AVR__
    EEPROM.setCommitASAP(false);
#endif
}

void ConfigManager::loop()
{
    if (!Serial)
        firehose = false;

#ifndef DISABLE_CMDS
    // gets all serial input while available
    while (Serial.available())
    {
        // get the new byte:
        char inChar = (char)Serial.read();
        // add it to the inputString:
        inputString += inChar;
        // if the incoming character is a newline, set a flag
        // so the main loop can do something about it:
        if (inChar == '\n')
        {
            stringComplete = true;
        }
    }

    // starts checking for each full string (string complete)
    if (stringComplete)
    {

        inputString.replace("\r", ""); // removes return chars
        inputString.replace("\n", ""); // removes newline char
        // checks if there are variable request and no command request list
        if (inputString.startsWith(CMD_WR))
        {
            uint8_t lastSep = 2;
            uint8_t nextSep = inputString.indexOf(CMD_SEP, lastSep + 1);
            // TODO this might go outside??
            rawCfgLen = strtoul(inputString.substring(lastSep + 1, nextSep).c_str(), NULL, 10);
            if (rawCfgLen == 0)
            {
                CMD_ERR(ERR_CMD_PARSE)
            }

            if (rawCfgLen > MAX_CFG_SIZE)
            {
                CMD_ERR(ERR_CFG_LEN)
            }
            lastSep = nextSep;
            // Serial.println(inputString.substring(lastSep + 1));
            rawCfgCRC = strtoul(inputString.substring(lastSep + 1).c_str(), NULL, 10);
            if (rawCfgCRC == 0)
            {
                CMD_ERR(ERR_CMD_PARSE)
            }
            ready();

            CRC32 crc_check;
            for (uint16_t i = 0; i < rawCfgLen; i++)
            {
                unsigned long now = millis();
                while (!Serial.available())
                {

                    if (max(millis() - now, 0) >= CMD_TIMEOUT)
                    {
                        CMD_ERR(ERR_CMD_TIMEOUT)
                    };
                }
                uint8_t byte = Serial.read();
                crc_check.update(byte);
                rawCfg[i] = byte;
            }

            uint32_t sum = crc_check.finalize();
            if (sum != rawCfgCRC)
            {
                CMD_ERR(ERR_CRC_MISMATCH)
            }
            // write our config bytes to eeprom
            if (!writeRawCfg())
            {
                CMD_ERR(ERR_EEPROM_WRITE)
            }
            // parse new config from eeprom
            if (!readConfig())
            {
                CMD_ERR(ERR_EEPROM_READ)
            }
        }
        else if (inputString.equals(CMD_RELOAD))
        {
            if (!readConfig())
            {
                CMD_ERR(ERR_EEPROM_READ)
            }
        }
        else if (inputString.equals(CMD_RD))
        {
            if (!readRawCfg())
            {
                CMD_ERR(ERR_EEPROM_READ)
            }

            printf(CMD_WR CMD_SEP "%u" CMD_SEP "%u\n", rawCfgLen, rawCfgCRC);

            String ready = Serial.readString();
            ready.replace("\r", "");
            ready.replace("\n", "");
            if (ready != CMD_READY)
            {
                CMD_ERR_MSG(ERR_CFG_SEND, "timeout, or invalid command")
            }

            if (Serial.write(rawCfg, rawCfgLen) < rawCfgLen)
            {
                CMD_ERR_MSG(ERR_CFG_SEND, "failed to send full config")
            }
            Serial.println();
        }
        else if (inputString.equals(CMD_BTN_TEST))
        {
            led.resume();
        }
        else if (inputString.startsWith(CMD_BTN_TEST))
        {
            uint8_t idx = strtoul(inputString.substring(sizeof(CMD_BTN_TEST)).c_str(), NULL, 10);

            if (!led.testLED(idx))
            {
                CMD_ERR(ERR_IDX_OUT_OF_RANGE)
            }
        }
        else if (inputString.equals(CMD_STATE))
        {
            input->sendState();
        }
        else if (inputString.equals(CMD_FIREHOSE))
        {
            firehose = true;
        }
        else if (inputString.equals(CMD_INF))
        {
            Serial.print("LEDSpy2");
        }
        // TODO: testLED stuff. also we can set directly now yay

        ok();
        // clear the flag
        stringComplete = false;
        // clear the string:
        inputString = "";
    }
#endif
    if (!configLED())
    {
        CMD_ERR(ERR_LED_CFG)
    }
    led.loop();
    if (firehose)
    {
        input->sendState();
    }
}

bool ConfigManager::configLED()
{
    if (led.getLastCFGCRC() == rawCfgCRC)
    {
        return true;
    }
    uint8_t mp[GENERIC_SPY_BTNS];
    for (uint8_t i = 0; i < GENERIC_SPY_BTNS; i++)
    {
        mp[i] = cfg.mappings.bytes[i];
    }

    led.reconfigure(cfg.brightness, cfg.max_brightness, mp, input, rawCfgCRC, cfg.min_brightness, numButtons(cfg.controller_type, cfg.variant));
    if (cfg.num_profiles > MAX_PROFILES)
    {
        // error(ERR_DECODE, "too many profiles");
        return false;
    }
    const pb_list_t *profs = (pb_list_t *)cfg.profiles.arg;
    for (uint8_t i = 0; i < cfg.num_profiles; i++)
    {
        led.addProfile(toLEDProfile(&((_Config_Profile *)profs->data)[i]));
    }
    for (uint8_t i = 0; i < cfg.switch_profiles.size; i++)
    {
        led.addSwitchProfile(cfg.switch_profiles.bytes[i]);
    }
    return true;
}
bool ConfigManager::defaultCfg()
{
    size_t newLen = defConfigLen;

    CRC32 crc_check;
    for (uint16_t i = 0; i < newLen; i++)
    {
        rawCfg[i] = defConfig[i];
        crc_check.update(rawCfg[i]);
    }

    uint32_t crc = crc_check.finalize();
    if (crc == rawCfgCRC && newLen == rawCfgLen)
    {
        return true;
    }
    rawCfgCRC = crc;
    rawCfgLen = newLen;

    return writeRawCfg();
}

bool ConfigManager::readRawCfg()
{
    uint16_t idx = 0;
    if (CONFIG_IS_ZERO(cfg))
    {
        char buf[SIG_LEN];
        EEPROM.get(idx, buf);
        if (!String(buf).equals(SIG))
        {
            if (!defaultCfg())
            {
                return false;
            }
        }
    }
    idx += SIG_LEN;
    uint16_t newCfgLen = 0;
    uint32_t newCRC = 0;
    EEPROM.get(idx, newCRC);
    if (newCRC == 0)
    {
        return false;
    }
    idx += sizeof(newCRC);
    EEPROM.get(idx, newCfgLen);
    if (newCfgLen == 0)
    {
        return false;
    }
    idx += sizeof(newCfgLen);
    rawCfgLen = newCfgLen;
    rawCfgCRC = newCRC;
    CRC32 crc_check;
    for (uint16_t i = 0; i < rawCfgLen; i++)
    {
        uint8_t byte = EEPROM.read(i + idx);
        crc_check.update(byte);
        rawCfg[i] = byte;
    }
    uint32_t sum = crc_check.finalize();
    if (sum != rawCfgCRC)
    {
        rawCfgCRC = 0;
        rawCfgLen = 0;
        return false;
    }
    return true;
}

bool ConfigManager::readConfig()
{
    if (!readRawCfg())
    {
        return false;
    }

    Config newCfg = Config_init_zero;
    _Config_Profile profs[MAX_PROFILES];
    pb_list_t *pf = new pb_list_t;
    pf->len = 0;
    pf->data = profs;
    newCfg.profiles.funcs.decode = &pbProfileDecode;
    newCfg.profiles.arg = pf;
    pb_istream_t pb_in = pb_istream_from_buffer(rawCfg, rawCfgLen);

    if (!pb_decode(&pb_in, Config_fields, &newCfg))
    {
        Serial.println(String(PB_GET_ERROR(&pb_in)));
        return false;
    }
    if (newCfg.controller_type != cfg.controller_type)
    {
        input = onChange(newCfg.controller_type);
    }
    cfg = newCfg;
    input->setControllerType(cfg.controller_type);
    input->setVariant(cfg.variant);

    return true;
}

bool ConfigManager::writeRawCfg()
{
    if (rawCfgLen == 0 || rawCfgCRC == 0)
    {
        return false;
    }

    uint16_t idx = 0;
    char buf[SIG_LEN];
    EEPROM.get(idx, buf);
    if (!String(buf).equals(SIG))
    {
        EEPROM.put(idx, SIG);
        idx += SIG_LEN;
    }
    else
    {
        idx += SIG_LEN;
        uint32_t crc = 0;
        EEPROM.get(idx, crc);
        // if the crc matches. no reason to write config again.
        if (crc == rawCfgCRC)
        {
            return true;
        }
    }
    EEPROM.put(idx, rawCfgCRC);
    idx += sizeof(rawCfgCRC);
    EEPROM.put(idx, rawCfgLen);
    idx += sizeof(rawCfgLen);
    for (uint16_t i = 0; i < rawCfgLen; i++)
    {
        EEPROM.update(i + idx, rawCfg[i]);
    }
#ifndef __AVR__
    if (!EEPROM.getCommitASAP())
    {
        EEPROM.commit();
    }
#endif
    return true;
}

void ConfigManager::error(error_t type, String msg)
{
    if (!Serial)
        return;
    if (msg == "")
    {
        printf(CMD_FAIL CMD_SEP "%d\n", type);
        return;
    }
    printf(CMD_FAIL CMD_SEP "%d" CMD_SEP "%s\n", type, msg);
}
void ConfigManager::ok(String msg)
{
    if (!Serial)
        return;
    if (msg == "")
    {
        Serial.println(CMD_OK);
        return;
    }
    Serial.println(CMD_OK CMD_SEP + msg);
}
void ConfigManager::ready(String msg)
{
    if (!Serial)
        return;
    if (msg == "")
    {

        Serial.println(CMD_READY);
        return;
    }
    Serial.println(CMD_READY CMD_SEP + msg);
}
